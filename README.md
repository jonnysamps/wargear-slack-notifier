# Wargear Slack Notifier

## Install Deps

`npm install`

## Configure

Copy the `.env.example` to `.env` and fill it out with appropriate values.  
Specifically, the `WARGEAR_USER_ID` needs to be someone in the game you care about notifications for.

## Run

`npm start`