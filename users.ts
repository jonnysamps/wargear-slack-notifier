export type SlackUser = {
  id: string
  name: string
  real_name: string
}

export const users: {[wargearId: string]: SlackUser} = {

  'b418a3a9c86b9afb9e6e069979c9e616': {
    id: 'U1JV9PS2G',
    name: 'mike',
    real_name: 'Michael Sealand',
  },
  '0f771c286f849d42f28124475dfc6827': {
    id: 'U1JVD4KB5',
    name: 'collin',
    real_name: 'Collin Brown',
  },
  'afead07c1a5ee1cbee812da3eb6ddf30': {
    id: 'U1K07KJS3',
    name: 'chase',
    real_name: 'Chase Cummings',
  },
  '7e5081a36beb8cc4f3ead4a863cb626d': {
    id: 'U9NHEHN3W',
    name: 'eric.achelis',
    real_name: 'Eric Achelis',
  },
  'a86ead953cce779623936a2efc3cd3c1': {
    id: 'U9ZEK8P41',
    name: 'kaylin.collins',
    real_name: 'Kaylin Collins',
  },
  'cbfd8e16045b88c6d457ada47a8a9d2f': {
    id: 'UAZ2DLTSA',
    name: 'jonathan.samples',
    real_name: 'Jonathan Samples',
  },
  'aedb276dcc11e64d5102a598232e4dda': {
    id: 'U014CFRBKHD',
    name: 'cody.boaz',
    real_name: 'Cody Boaz',
  },
  '457676605adfcb3532b89c2fe835ea94': {
    id: 'U0155SH0G9F',
    name: 'jensen.shurbert',
    real_name: 'Jensen Shurbert',
  },
  'e385a754a0ba51d48995c72f394f4b43': {
    id: 'U0EH72RKL',
    real_name: 'erick',
    name: 'erick'
  }
};