require('dotenv').config();
import * as Pusher from 'pusher-js/node';
import { WebClient } from '@slack/web-api';
import { users, SlackUser } from './users';

const SLACK_TOKEN = process.env.SLACK_TOKEN;
const WARGEAR_USER_ID = process.env.WARGEAR_USER_ID;

async function postToSlack(user: SlackUser, message: string) {
  const slack = new WebClient(SLACK_TOKEN);
  try {
    await slack.chat.postMessage({
      channel: user.id,
      text: message,
      icon_emoji: ':wargear',
      link_names: true
    });
  } catch (e) {
    console.error('Error', JSON.stringify(e, null, 2));
  }
}

async function getSlackUsers() {
  const slack = new WebClient(SLACK_TOKEN);
  try {
    let result = await slack.users.list();
    console.log('slack post result: ', result);
  } catch (e) {
    console.error('Error', JSON.stringify(e, null, 2));
  }
}

type Player = {
    "id": string,
    "color_name": string,
    "color_code": string,
    "text_code": string,
    "seat": number,
    "status": string,
    "team": null,
    "turn_counter": number,
    "autoboot_next_turn": number,
    "username": string
};

type EventPayload = {
  "gameid": string,
  "gamestatus": string,
  "name": string,
  "boardid": string,
  "favorite": number,
  "boardname": string,
  "image_extension": string,
  "boot_time": string,
  "delay_time": string,
  "num_players": string,
  "current_turn": string,
  "turnstamp": number,
  "boot_clock": null,
  "current_turns": Number[],
  "mytime": number,
  "beaconid": number,
  "players": { [turnOrder:string]: Player }
  "createtime": string
}

function getSlackUserByWGID(id: string) {
  return users[id];
}


async function main() {
  // await getSlackUsers();
  // process.exit();
  Pusher.logToConsole = true;
  const pusher = new Pusher(WARGEAR_USER_ID); // UserId of someone in the game
  // const broadcastChannel = pusher.subscribe('broadcast');
  // broadcastChannel.bind('new_game', (...args) => { console.log ({new_game: args})});
  // broadcastChannel.bind('system_message', (...args) => { console.log ({system_message: args})})
  // broadcastChannel.bind('ticker', (...args) => { console.log ({ticker: args})})

  const uid = 'cbfd8e16045b88c6d457ada47a8a9d2f'; //'jonnysamps';
  const userChannel = pusher.subscribe('uid_' + uid);

  let lastTurnID = null;
  const events = [
    'game_event',
    'turn_start',
    'turn_end',
    'clock_update',
    'achievement',
    'rank_change'
  ];
  events.forEach(e => {
    userChannel.bind(e, (payload: EventPayload) => {
      const user = getSlackUserByWGID(payload.current_turn);
      if(user && lastTurnID !== payload.current_turn){
        lastTurnID = payload.current_turn;
        console.log(`🥳 Turn change: ${user.real_name} 🥳`);
        postToSlack(
          user, 
          `It is now your turn on Wargear: ${payload.name}\nhttp://www.wargear.net/games/player/${payload.gameid}`
          );
      }
    });
  });
}

main();
